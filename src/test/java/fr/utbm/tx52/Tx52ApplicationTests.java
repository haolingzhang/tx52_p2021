package fr.utbm.tx52;

import fr.utbm.tx52.entity.PatientEntity;
import fr.utbm.tx52.repository.ActRepository;
import fr.utbm.tx52.repository.PatientRepository;
import fr.utbm.tx52.repository.SoignantRepository;
import fr.utbm.tx52.service.PatientService;
import fr.utbm.tx52.service.SoignantService;
import fr.utbm.tx52.util.Md5Util;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;


@SpringBootTest
class Tx52ApplicationTests {

	@Autowired
	SoignantService soignantService;

	@Autowired
	PatientService patientService;

	@Autowired
	PatientRepository patientRepository;


	@Autowired
	ActRepository actRepository;
	@Test
	void contextLoads() {
	}


//	@Test
//	void findSoignantById() {
//		System.out.println(soignantService.findSoignantById((long)1));
//	}

	@Test
	void findPatient(){
		System.out.println(patientService.findPatientByNom("name3"));
	}

	@Test
	void findSoignantByEmail() {
		System.out.println(soignantService.getSoignantByEmail("t@t.com"));
	}

	@Test
	void actTest() {
		System.out.println(actRepository.findActByOrdonnanceIdAndActName((long)2,"1"));
	}


	@Test
	void searchHeurePassage() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate localDate = LocalDate.parse("2021-05-21", formatter);
		Instant instant = localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
		java.util.Date utilDate = java.util.Date.from(instant);

		//System.out.println(patientService.findPatientByHeurePassage(utilDat));
	}

	@Test
	void testMd5(){
		String s = new String("4r3e2w1q$R#E@W!Q");
		System.out.println("原始：" + s);
		System.out.println("MD5后：" + Md5Util.string2MD5(s));
		System.out.println("加密的：" + Md5Util.convertMD5(s));
		System.out.println("解密的：" + Md5Util.convertMD5(Md5Util.convertMD5(s)));
		System.out.println("1111->" + Md5Util.string2MD5(Md5Util.string2MD5(s)));
		String pwd = new String("123456");
		System.out.println("加密的：" + Md5Util.convertMD5(pwd));
	}


}
