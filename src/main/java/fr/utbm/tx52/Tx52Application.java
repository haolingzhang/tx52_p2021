package fr.utbm.tx52;

import jdk.internal.instrumentation.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.util.StringUtils;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.PostConstruct;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.TimeZone;

@Slf4j
@EnableJpaAuditing
@SpringBootApplication
public class Tx52Application {

	@PostConstruct
	public void init(){
		TimeZone.setDefault(TimeZone.getTimeZone("GMT+2:00"));   // It will set UTC timezone
		System.out.println("Spring boot application running in UTC timezone :"+new Date());   // It will print UTC timezone
	}

	public static void main(String[] args) throws UnknownHostException {


		//SpringApplication.run(Tx52Application.class, args);


		ConfigurableApplicationContext application = SpringApplication.run(Tx52Application.class, args);
		Environment env = application.getEnvironment();
		String ip = InetAddress.getLocalHost().getHostAddress();
		String port = env.getProperty("server.port");
		String path = env.getProperty("server.servlet.context-path");
		if (StringUtils.isEmpty(path)) {
			path = "";
		}
		System.out.println("\n----------------------------------------------------------\n\t" +
				"Application  is running! Access URLs:\n\t" +
				"Local site: \t\thttp://localhost:" + port + path + "\n\t" +
				"External site: \thttp://" + ip + ":" + port + path + "\n\t" +
				"----------------------------------------------------------");


	}
}
