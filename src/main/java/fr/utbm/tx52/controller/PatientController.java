package fr.utbm.tx52.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import fr.utbm.tx52.entity.PatientDetailEntity;
import fr.utbm.tx52.entity.PatientEntity;
import fr.utbm.tx52.entity.ResultEntity;
import fr.utbm.tx52.repository.PatientDetailRepository;
import fr.utbm.tx52.repository.PatientRepository;
import fr.utbm.tx52.service.PatientService;
import fr.utbm.tx52.util.BaseResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/patient")
public class PatientController {

    @Autowired
    PatientService patientService;

    @Autowired
    PatientRepository patientRepository;


    @Autowired
    PatientDetailRepository patientDetailRepository;


    //for the administrator
    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public ResultEntity searchPatientByNom(@RequestParam("nom") String nom){
        try{
            if(nom.isEmpty()){
                throw new Exception("please enter the nom of patient");
            }
            List<PatientEntity> saveResult = patientService.findPatientByNom(nom);
            return BaseResultUtil.resSuccess(saveResult);
        }catch(Exception e) {
            return BaseResultUtil.resFailed("failed to find message of this patient！");
        }
    }

    @RequestMapping(value = "/searchById", method = RequestMethod.GET)
    public ResultEntity searchPatientById(@RequestParam("patientId") Long patientId){
        try{
            if( patientId == null ){
                throw new Exception("please enter the id of patient");
            }
            PatientEntity saveResult = patientRepository.findById(patientId).get();
            return BaseResultUtil.resSuccess(saveResult);
        }catch(Exception e) {
            return BaseResultUtil.resFailed("failed to find message of this patient！");
        }
    }

    //for the soignant
    @RequestMapping(value = "/searchBySoignant", method = RequestMethod.GET)
    public ResultEntity searchPatientByNom(@RequestParam("nom") String nom,
                                           @RequestParam("soignantId") Long soignantId){
        try{
            if(nom.isEmpty()){
                throw new Exception("please enter the nom of patient");
            }
            List<PatientEntity> saveResult = patientService.findPatientByNomAndSoignant(nom,soignantId);
            return BaseResultUtil.resSuccess(saveResult);
        }catch(Exception e) {
            return BaseResultUtil.resFailed("failed to find message of this patient！");
        }
    }

    @RequestMapping(value = "/searchAllBySoignant", method = RequestMethod.GET)
    public ResultEntity searchAllByNom(@RequestParam("soignantId") Long soignantId){
        try{
            List<PatientEntity> saveResult = patientService.findPatientBySoignantId(soignantId);
            return BaseResultUtil.resSuccess(saveResult);
        }catch(Exception e) {
            return BaseResultUtil.resFailed("failed to find message of this patient！");
        }
    }

    @RequestMapping(value = "/searchDetail", method = RequestMethod.GET)
    public ResultEntity searchPatientByNom(@RequestParam("patientId") Long patientId){
        try{
            List<PatientDetailEntity> saveResult = patientDetailRepository.findPatientDetailByPatientId(patientId);
            return BaseResultUtil.resSuccess(saveResult);
        }catch(Exception e) {
            return BaseResultUtil.resFailed("failed to find message of this patient！");
        }
    }

//    @RequestMapping(value = "/searchByHeurePassage", method = RequestMethod.GET)
//    public ResultEntity searchPatientB(@RequestParam("heurePassage") String heurePassage){
//        try{
//            SimpleDateFormat fm=new SimpleDateFormat("yyyyMMdd HH:mm:ss");
//            SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
//           // DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
//           // DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
//           // LocalDate localDate = LocalDate.parse(heurePassage, formatter);
//           // Instant instant = localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
//         //   java.util.Date utilDate = java.util.Date.from(instant);
//            Date date1=fm.parse(fm.format(sdf.parse(heurePassage).getTime()));
//            Date date2=fm.parse(fm.format(sdf.parse(heurePassage).getTime()+86400000));
//            List<PatientEntity> saveResult = patientService.findPatientByHeurePassage(date1,date2);
//            return BaseResultUtil.resSuccess(saveResult);
//        }catch(Exception e) {
//            return BaseResultUtil.resFailed("failed to find message of patients！");
//        }
//    }


    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public ResultEntity savePatient( @RequestBody String patientInfo){
        try{
            JSONObject jsonParams=JSONObject.parseObject(patientInfo);

            //test si les information sont recu par backend
            System.out.println(jsonParams);

            //update patient info
            JSONObject jsonpatient=jsonParams.getJSONObject("patient");
            String id = jsonpatient.getString("id");
            String commentaire = jsonpatient.getString("commentaire");
            //Date heurePassage = jsonpatient.getDate("heurePassage");
            String numeroSociete =  jsonpatient.getString("numeroSociete");
            String email =  jsonpatient.getString("email");
            String phone =  jsonpatient.getString("phone");
            String address =  jsonpatient.getString("address");
            String sexe =  jsonpatient.getString("sexe");
            String age =  jsonpatient.getString("age");
            String prenom =  jsonpatient.getString("prenom");
            String nom =  jsonpatient.getString("nom");
            Long patientId = Long.parseLong(id);
            PatientEntity patient = patientRepository.findById(patientId).get();
            patient.setPrenom(prenom);
           // patient.setHeurePassage(heurePassage);
            patient.setNom(nom);
            patient.setCommentaire(commentaire);
            patient.setAddress(address);
            patient.setSexe(sexe);
            patient.setNumeroSociete(numeroSociete);
            patient.setEmail(email);
            patient.setPhone(phone);
            patient.setAge(age);
            patientRepository.save(patient);

            //patient details
            JSONArray patientDetailLists = jsonpatient.getJSONArray("patientDetail");
            for (Object patientDetail : patientDetailLists){
                JSONObject patientDetailJson = (JSONObject) patientDetail;
                if(patientDetailJson==null){
                    continue;
                }
                String variable = patientDetailJson.getString("variable");
                String variableValue = patientDetailJson.getString("variableValue");
                PatientDetailEntity patientDetailEntity = patientDetailRepository.
                                                          findPatientDetailByPatientIdAndVariable(patientId,variable);
                if(null == patientDetailEntity || patientDetailEntity.toString().equals("")){
                    PatientDetailEntity patientDetailEntity1 = new PatientDetailEntity();
                    patientDetailEntity1.setPatiendId(patientId);
                    patientDetailEntity1.setVariable(variable);
                    patientDetailEntity1.setVariableValue(variableValue);
                    patientDetailRepository.save(patientDetailEntity1);
                }else{
                patientDetailEntity.setVariableValue(variableValue);
                patientDetailRepository.save(patientDetailEntity);}
            }
            return BaseResultUtil.resSuccess("success registre for: "+patient.getNom()+" "+patient.getPrenom());
        }catch(Exception e) {
            return BaseResultUtil.resFailed("failed to update the information about patient！"+e.getMessage());
        }
    }




}
