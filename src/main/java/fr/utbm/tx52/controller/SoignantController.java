package fr.utbm.tx52.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import fr.utbm.tx52.entity.*;
import fr.utbm.tx52.repository.SoignantRepository;
import fr.utbm.tx52.repository.SpecialiteRepository;
import fr.utbm.tx52.repository.SpecialiteSoignantRepository;
import fr.utbm.tx52.service.SoignantService;
import fr.utbm.tx52.util.BaseResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@CrossOrigin
@RestController
@RequestMapping("/api/soignant")
public class SoignantController {

    @Autowired
    SoignantService soignantService;

    @Autowired
    SoignantRepository soignantRepository;

    @Autowired
    SpecialiteSoignantRepository specialiteSoignantRepository;

    @Autowired
    SpecialiteRepository specialiteRepository;


    @RequestMapping(value = "/searchSoignantByEmail", method = RequestMethod.GET)
    public ResultEntity searchSoignantByEmail(@RequestParam("soignantEmail") String soignantEmail){
        try{
            if(soignantEmail.isEmpty()){
                throw new Exception("please enter EMAIL of soignant");
            }
            SoignantEntity saveResult = soignantService.getSoignantByEmail(soignantEmail);
            return BaseResultUtil.resSuccess(saveResult);
        }catch(Exception e) {
            return BaseResultUtil.resFailed("failed to find message of this soignant！"+e.getMessage());
        }
    }

    @RequestMapping(value = "/searchSoignantById", method = RequestMethod.GET)
    public ResultEntity searchSoignantById(@RequestParam("soignantId") Long soignantId){
        try{
            if(soignantId ==null ){
                throw new Exception("please enter id of soignant");
            }
            SoignantEntity saveResult = soignantRepository.findById(soignantId).get();
            return BaseResultUtil.resSuccess(saveResult);
        }catch(Exception e) {
            return BaseResultUtil.resFailed("failed to find message of this soignant！"+e.getMessage());
        }
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResultEntity searchSoignant(){
        try{
            List<SoignantEntity> saveResult = soignantRepository.findAll();
            return BaseResultUtil.resSuccess(saveResult);
        }catch(Exception e) {
            return BaseResultUtil.resFailed("failed to find message of this soignant！"+e.getMessage());
        }
    }

    @RequestMapping(value = "/specialiste", method = RequestMethod.GET)
    public ResultEntity searchSpecialiste(@RequestParam("soignantId") Long soignantId){
        try{
            List<SpecialiteEntity> saveResult = new ArrayList<>();
            List<Long> specialiteLists = specialiteSoignantRepository.findSpecialisteIdBySoignantId(soignantId);
            for(long specialite:specialiteLists){
                saveResult.add(specialiteRepository.findById(specialite).get());
            }
            return BaseResultUtil.resSuccess(saveResult);
        }catch(Exception e) {
            return BaseResultUtil.resFailed("failed to find specialiste of this soignant！"+e.getMessage());
        }
    }


    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public ResultEntity saveSoignant( @RequestBody String soignantInfo) {
        try {
            JSONObject jsonParams = JSONObject.parseObject(soignantInfo);

            //test si les information sont recu par backend
            System.out.println(jsonParams);

            //update soignant info
            JSONObject jsonSoignant = jsonParams.getJSONObject("soignant");
            String id = jsonSoignant.getString("id");
            String email = jsonSoignant.getString("email");
            String phone = jsonSoignant.getString("phone");
            String address = jsonSoignant.getString("address");
            String prenom = jsonSoignant.getString("prenom");
            String nom = jsonSoignant.getString("nom");
            Long soignantId = Long.parseLong(id);
            SoignantEntity soignant = soignantRepository.findById(soignantId).get();
            soignant.setPrenom(prenom);
            soignant.setNom(nom);
            soignant.setAddress(address);
            soignant.setEmail(email);
            soignant.setPhone(phone);
            soignantRepository.save(soignant);

            //soignant specialist
            JSONArray soignantSpecialisteJsons = jsonSoignant.getJSONArray("specialite");
            for (Object soignantSpecialisteObject : soignantSpecialisteJsons) {
                JSONObject soignantSpecialisteJson = (JSONObject) soignantSpecialisteObject;
                if (soignantSpecialisteJson == null) {
                    continue;
                }
                String specialiteId = soignantSpecialisteJson.getString("id");
                String specialiteName = soignantSpecialisteJson.getString("specialiteName");
                String specialiteDescription = soignantSpecialisteJson.getString("specialiteDescription");
                Long specialiteIdL = Long.parseLong(specialiteId);
                SpecialiteSoignantEntity specialiteSoignantEntity = specialiteSoignantRepository.
                                                                    findBySoignantIdAndSpecialiteId(soignantId,specialiteIdL);
                if(specialiteSoignantEntity==null||specialiteSoignantEntity.toString().equals("")){
                    SpecialiteSoignantEntity specialiteSoignantEntity1 = new SpecialiteSoignantEntity();
                    specialiteSoignantEntity1.setSoignantId(soignantId);
                    specialiteSoignantEntity1.setSpecialiteId(specialiteIdL);
                    specialiteSoignantRepository.save(specialiteSoignantEntity1);
                }

            }
            return BaseResultUtil.resSuccess("success registre for: " + soignant.getNom()+" "+soignant.getPrenom());
        } catch (Exception e) {
            return BaseResultUtil.resFailed("failed to update the information about soignant！" + e.getMessage());
        }
    }

    @RequestMapping(value = "/listSpecialists", method = RequestMethod.GET)
    public ResultEntity listSpecialists(){
        try{
            List<SpecialiteEntity> saveResult = specialiteRepository.findAll();
            return BaseResultUtil.resSuccess(saveResult);
        }catch(Exception e) {
            return BaseResultUtil.resFailed("failed to find message of all the specialists！"+e.getMessage());
        }
    }



}
