package fr.utbm.tx52.controller;

import com.alibaba.fastjson.JSONArray;
import fr.utbm.tx52.entity.ActEntity;
import fr.utbm.tx52.entity.OrdonnanceEntity;
import fr.utbm.tx52.entity.PatientEntity;
import fr.utbm.tx52.entity.ResultEntity;
import fr.utbm.tx52.repository.OrdonnanceRepository;
import fr.utbm.tx52.repository.PatientRepository;
import fr.utbm.tx52.service.ActService;
import fr.utbm.tx52.service.OrdonnanceService;
import fr.utbm.tx52.util.BaseResultUtil;
import fr.utbm.tx52.util.JsonUtils;
//import net.minidev.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.crypto.Data;
import java.text.SimpleDateFormat;
import java.util.*;

@CrossOrigin
@RestController
@RequestMapping("/api/ordonnance")
public class OrdonnanceController {

    @Autowired
    OrdonnanceRepository ordonnanceRepository;

    @Autowired
    PatientRepository patientRepository;

    @Autowired
    OrdonnanceService ordonnanceService;

    @Autowired
    ActService actService;

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public ResultEntity searchOrdonnanceById(@RequestParam("ordonnanceId") Long ordonnanceId){
        try{
            if(ordonnanceId==null){
                throw new Exception("you didn't choose a ordonnance");
            }
            OrdonnanceEntity saveResult=ordonnanceRepository.findById(ordonnanceId).get();
            return BaseResultUtil.resSuccess(saveResult);
        }catch(Exception e) {
            return BaseResultUtil.resFailed("failed to find message of this ordonnance！");
        }
    }

    @RequestMapping(value = "/searchByPatient", method = RequestMethod.GET)
    public ResultEntity searchOrdonnanceByPatientId(@RequestParam("patientId") Long patientId){
        try{
            if(patientId==null){
                throw new Exception("you didn't choose a patient");
            }
            List<OrdonnanceEntity> saveResult=ordonnanceService.findOrdonnanceByPatientId(patientId);
            return BaseResultUtil.resSuccess(saveResult);
        }catch(Exception e) {
            return BaseResultUtil.resFailed("failed to find message of this patient！");
        }
    }

    @RequestMapping(value = "/searchAct", method = RequestMethod.GET)
    public ResultEntity searchActByOrdonnanceId(@RequestParam("ordonnanceId") Long ordonnanceId){
        try{
            if(ordonnanceId==null){
                throw new Exception("you didn't choose a ordonnance");
            }
            List<ActEntity> saveResult=actService.findActByOrdonnanceId(ordonnanceId);
            return BaseResultUtil.resSuccess(saveResult);
        }catch(Exception e) {
            return BaseResultUtil.resFailed("failed to find message of this ordonnance！");
        }
    }

    @RequestMapping(value = "/save", method = {RequestMethod.POST, RequestMethod.GET})
    public ResultEntity saveOrdonnance( @RequestBody String ordonnanceInfo){
        try{
            HashMap<String, String > actMaps= new HashMap<>();
            OrdonnanceEntity ordonnanceEntity;
            JSONObject jsonParams=JSONObject.parseObject(ordonnanceInfo);
            System.out.println(jsonParams);
            JSONObject jsonOrdonnance=jsonParams.getJSONObject("ordonnance");
            String id = jsonOrdonnance.getString("id");
            String commentaire = jsonOrdonnance.getString("commentaire");
            Date heurePassage = jsonOrdonnance.getDate("heurePassage");
            JSONArray acts = jsonOrdonnance.getJSONArray("acte");
            String soignantId = jsonOrdonnance.getString("soignantId");
            String patientId =  jsonOrdonnance.getString("patientId");
            for (Object act : acts){
                JSONObject actJson = (JSONObject) act;
                if(act==null){
                    continue;
                }
                actMaps.put(actJson.getString("actDescription"),actJson.getString("actCommentaire"));
            }
            ordonnanceEntity= ordonnanceService.saveOrdonnance(commentaire,id,actMaps
                                                                ,soignantId,patientId,heurePassage);

            return BaseResultUtil.resSuccess("success registre: "+ordonnanceEntity);
        }catch(Exception e) {
            return BaseResultUtil.resFailed("failed to create ordonnance！"+e.getMessage());
        }
    }

    @RequestMapping(value = "/searchByHeurePassage", method = RequestMethod.GET)
    public ResultEntity searchPatientB(@RequestParam("heurePassage") String heurePassage){
        try{
            SimpleDateFormat fm=new SimpleDateFormat("yyyyMMdd HH:mm:ss");
            SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
           List<Object> saveResultList = new ArrayList<>();
            Date date1=fm.parse(fm.format(sdf.parse(heurePassage).getTime()));
            Date date2=fm.parse(fm.format(sdf.parse(heurePassage).getTime()+86400000));
            List<OrdonnanceEntity> ordonnanceLists = ordonnanceService.findOrdonnanceByHeurePassage(date1,date2);
            for(OrdonnanceEntity ordonnance:ordonnanceLists){
                HashMap<String,Object> saveResult = new HashMap<>();
                Long patientId = ordonnance.getPatientId();
                PatientEntity patient = patientRepository.findById(patientId).get();
                Date heurePassagePaitent = ordonnance.getHeurePassage();
                saveResult.put("patient",patient);
                saveResult.put("heurePassage",heurePassagePaitent);
                saveResultList.add(saveResult);
            }
            return BaseResultUtil.resSuccess(saveResultList);
        }catch(Exception e) {
            return BaseResultUtil.resFailed("failed to find message of patients！");
        }
    }
}
