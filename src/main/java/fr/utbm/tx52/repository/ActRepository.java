package fr.utbm.tx52.repository;

import fr.utbm.tx52.entity.ActEntity;
import fr.utbm.tx52.entity.OrdonnanceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ActRepository extends JpaRepository<ActEntity, Long>, JpaSpecificationExecutor<ActEntity> {

    @Query(value = "select * from tb_act where ordonnance_id = ?1", nativeQuery = true)
    List<ActEntity> findActByOrdonnanceId(Long ordonnanceId);

    @Query(value = "select * from tb_act where ordonnance_id = ?1 and act_description = ?2", nativeQuery = true)
    ActEntity findActByOrdonnanceIdAndActName(Long ordonnanceId, String actDescription);

}
