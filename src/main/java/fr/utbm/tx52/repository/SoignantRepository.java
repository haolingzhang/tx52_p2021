package fr.utbm.tx52.repository;

import fr.utbm.tx52.entity.SoignantEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface SoignantRepository extends JpaRepository<SoignantEntity, Long>, JpaSpecificationExecutor<SoignantEntity> {
    /**
     * search soignant according to its id
     * @param id
     * @return
     */
    @Query(value = "select * from tb_soignant s where s.id = ?1", nativeQuery = true)
    SoignantEntity findSoignantById(Long id);

    @Query(value = "select s.password from tb_soignant s where s.email = ?1", nativeQuery = true)
    String getSoignantPasswordByEmail(String soignantEmail);

    @Query(value = "select * from tb_soignant s where s.email = ?1", nativeQuery = true)
    SoignantEntity getSoignantByEmail(String soignantEmail);


}
