package fr.utbm.tx52.repository;

import fr.utbm.tx52.entity.SpecialiteEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface SpecialiteRepository extends JpaRepository<SpecialiteEntity, Long>, JpaSpecificationExecutor<SpecialiteEntity> {



}
