package fr.utbm.tx52.repository;

import fr.utbm.tx52.entity.OrdonnanceEntity;
import fr.utbm.tx52.entity.PatientDetailEntity;
import fr.utbm.tx52.entity.PatientEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;


@Repository
public interface OrdonnanceRepository extends JpaRepository<OrdonnanceEntity, Long>, JpaSpecificationExecutor<OrdonnanceEntity> {

    @Query(value = "select * from tb_ordonnance where patient_id = ?1", nativeQuery = true)
    List<OrdonnanceEntity> findOrdonnanceByPatientId(Long patientId);

    @Query(value = "select * from tb_ordonnance where heure_passage between ?1 and ?2", nativeQuery = true)
    List<OrdonnanceEntity> findOrdonnanceByHeurePassage(Date date1, Date date2);

}
