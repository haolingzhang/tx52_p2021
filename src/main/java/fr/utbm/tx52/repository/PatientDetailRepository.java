package fr.utbm.tx52.repository;

import fr.utbm.tx52.entity.PatientDetailEntity;
import fr.utbm.tx52.entity.SoignantEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PatientDetailRepository extends JpaRepository<PatientDetailEntity, Long>, JpaSpecificationExecutor<PatientDetailEntity> {

    @Query(value = "select * from tb_patient_detail where patient_id = ?1", nativeQuery = true)
    List<PatientDetailEntity> findPatientDetailByPatientId(Long patientId);

    @Query(value = "select * from tb_patient_detail where patient_id = ?1 and variable=?2", nativeQuery = true)
    PatientDetailEntity findPatientDetailByPatientIdAndVariable(Long patientId,String variable);



}
