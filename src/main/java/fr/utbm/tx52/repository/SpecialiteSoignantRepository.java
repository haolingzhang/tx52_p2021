package fr.utbm.tx52.repository;

import fr.utbm.tx52.entity.OrdonnanceEntity;
import fr.utbm.tx52.entity.SpecialiteSoignantEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SpecialiteSoignantRepository  extends JpaRepository<SpecialiteSoignantEntity, Long>, JpaSpecificationExecutor<SpecialiteSoignantEntity> {

    @Query(value = "select specialite_id from tb_specialite_soignant where soignant_id = ?1", nativeQuery = true)
    List<Long> findSpecialisteIdBySoignantId(Long soignantId);

    @Query(value = "select * from tb_specialite_soignant where soignant_id = ?1 and specialite_id=?2", nativeQuery = true)
    SpecialiteSoignantEntity findBySoignantIdAndSpecialiteId(Long soignantId,Long specialiteId);

}
