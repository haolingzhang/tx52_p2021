package fr.utbm.tx52.repository;

import fr.utbm.tx52.entity.PatientEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface PatientRepository extends JpaRepository<PatientEntity, Long>, JpaSpecificationExecutor<PatientEntity> {

    @Query(value = "select * from tb_patient where nom = ?1", nativeQuery = true)
    List<PatientEntity> findPatientByNom(String nom);

    @Query(value = "select * from tb_patient where nom = ?1 and soignant_id=?2", nativeQuery = true)
    List<PatientEntity> findPatientByNomAndSoignant(String nom,Long soignantId);

    @Query(value = "select * from tb_patient where soignant_id=?1", nativeQuery = true)
    List<PatientEntity> findPatientBySoignant(Long soignantId);

//    @Query(value = "select * from tb_patient where heure_passage between ?1 and ?2", nativeQuery = true)
//    List<PatientEntity> findPatientByHeurePassage(Date date1,Date date2);

}
