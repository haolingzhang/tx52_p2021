package fr.utbm.tx52.service.impl;

import fr.utbm.tx52.entity.PatientEntity;
import fr.utbm.tx52.repository.PatientRepository;
import fr.utbm.tx52.service.PatientService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Slf4j
@Service
public class PatientServiceImpl implements PatientService {

    @Autowired
    PatientRepository patientRepository;

    //find patient by nom
    @Override
    public List<PatientEntity> findPatientByNom(String nom) {
        return patientRepository.findPatientByNom(nom);
    }

    @Override
    public List<PatientEntity> findPatientByNomAndSoignant(String nom,Long SoignantId) {
        return patientRepository.findPatientByNomAndSoignant(nom,SoignantId);
    }

    @Override
    public List<PatientEntity> findPatientBySoignantId(Long SoignantId) {
        return patientRepository.findPatientBySoignant(SoignantId);
    }

//    @Override
//    public List<PatientEntity> findPatientByHeurePassage(Date heurePassage,Date date2) {
//        return patientRepository.findPatientByHeurePassage(heurePassage,date2);
//    }

}
