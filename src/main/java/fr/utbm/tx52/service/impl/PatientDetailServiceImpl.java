package fr.utbm.tx52.service.impl;

import fr.utbm.tx52.entity.PatientDetailEntity;
import fr.utbm.tx52.entity.PatientEntity;
import fr.utbm.tx52.repository.PatientDetailRepository;
import fr.utbm.tx52.repository.PatientRepository;
import fr.utbm.tx52.service.PatientDetailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class PatientDetailServiceImpl implements PatientDetailService {
    @Autowired
    PatientDetailRepository patientDetailRepository;

    @Override
    public List<PatientDetailEntity> findPatientDetailByPatientId(Long patientId) {
        return patientDetailRepository.findPatientDetailByPatientId(patientId);
    }



}
