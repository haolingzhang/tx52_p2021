package fr.utbm.tx52.service.impl;

import fr.utbm.tx52.entity.SoignantEntity;
import fr.utbm.tx52.repository.PatientRepository;
import fr.utbm.tx52.repository.SoignantRepository;
import fr.utbm.tx52.service.SoignantService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class SoignantServiceImpl implements SoignantService {
    @Autowired
    SoignantRepository soignantRepository;

    @Override
    public String getSoignantPasswordByEmail(String soignantEmail){
       return soignantRepository.getSoignantPasswordByEmail(soignantEmail);
    }

    @Override
    public SoignantEntity getSoignantByEmail(String soignantEmail){
        return soignantRepository.getSoignantByEmail(soignantEmail);
    }
}
