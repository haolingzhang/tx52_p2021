package fr.utbm.tx52.service.impl;

import fr.utbm.tx52.entity.ActEntity;
import fr.utbm.tx52.entity.OrdonnanceEntity;
import fr.utbm.tx52.entity.PatientDetailEntity;
import fr.utbm.tx52.repository.ActRepository;
import fr.utbm.tx52.repository.OrdonnanceRepository;
import fr.utbm.tx52.repository.PatientDetailRepository;
import fr.utbm.tx52.service.OrdonnanceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Slf4j
@Service
public class OrdonnanceServiceImpl implements OrdonnanceService {
    @Autowired
    OrdonnanceRepository ordonnanceRepository;

    @Autowired
    ActRepository actRepository;


    @Override
    public List<OrdonnanceEntity> findOrdonnanceByPatientId(Long patientId) {
        return ordonnanceRepository.findOrdonnanceByPatientId(patientId);
    }

    @Override
    public OrdonnanceEntity saveOrdonnance(String commentaire, String id , HashMap<String, String > actMaps,
                                           String soignantId, String patientId, Date heurePassage) {
        OrdonnanceEntity ordonnanceEntity = new OrdonnanceEntity();

        Long ordonnanceId = Long.parseLong(id);
        Long patientIdL = Long.parseLong(patientId);
        Long soignantIdL = Long.parseLong(soignantId);
        ordonnanceEntity.setId(ordonnanceId);
        ordonnanceEntity.setCommentaire(commentaire);
        ordonnanceEntity.setPatientId(patientIdL);
        ordonnanceEntity.setSoignantId(soignantIdL);
        ordonnanceEntity.setHeurePassage(heurePassage);
        System.out.println("ordonnance service impl");
        actMaps.forEach((key, value) -> {
//            System.out.println("key:  "+key);
//            System.out.println("value:  "+value);
//            System.out.println("ordonnanceid :  "+ordonnanceId);
            ActEntity actEntity = actRepository.findActByOrdonnanceIdAndActName(ordonnanceId,key);
//            System.out.println(actEntity);
            actEntity.setActCommentaire(value);
//            System.out.println("save act ");
            actRepository.save(actEntity);
        });
//        System.out.println("save ordonnance ");
        ordonnanceRepository.save(ordonnanceEntity);
        return ordonnanceEntity;
    }

    @Override
    public List<OrdonnanceEntity> findOrdonnanceByHeurePassage(Date date1, Date date2) {
        return ordonnanceRepository.findOrdonnanceByHeurePassage(date1,date2);
    }


}
