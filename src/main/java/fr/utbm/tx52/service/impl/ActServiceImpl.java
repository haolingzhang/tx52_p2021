package fr.utbm.tx52.service.impl;

import fr.utbm.tx52.entity.ActEntity;
import fr.utbm.tx52.repository.ActRepository;
import fr.utbm.tx52.service.ActService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class ActServiceImpl implements ActService {

    @Autowired
    ActRepository actRepository;

    @Override
    public List<ActEntity> findActByOrdonnanceId(Long ordonnanceId){
        return actRepository.findActByOrdonnanceId(ordonnanceId);
    }

    @Override
    public ActEntity findActByOrdonnanceIdAndActName(Long ordonnanceId, String actDescription) {
        return actRepository.findActByOrdonnanceIdAndActName(ordonnanceId,actDescription);
    }
}
