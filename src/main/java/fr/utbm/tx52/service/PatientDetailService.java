package fr.utbm.tx52.service;

import fr.utbm.tx52.entity.PatientDetailEntity;

import java.util.List;

public interface PatientDetailService {
    public List<PatientDetailEntity> findPatientDetailByPatientId(Long patientId);

}
