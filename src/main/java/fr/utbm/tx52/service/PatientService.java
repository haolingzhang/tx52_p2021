package fr.utbm.tx52.service;

import fr.utbm.tx52.entity.PatientEntity;

import java.util.Date;
import java.util.List;

public interface PatientService {


    //find patient by nom for administrator
    public List<PatientEntity> findPatientByNom(String nom);

    //find patient by nom for soignant
    public List<PatientEntity> findPatientByNomAndSoignant(String nom,Long SoignantId);

    public List<PatientEntity> findPatientBySoignantId(Long SoignantId);

//    public List<PatientEntity> findPatientByHeurePassage(Date date1,Date date2);

}
