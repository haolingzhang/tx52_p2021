package fr.utbm.tx52.service;

import fr.utbm.tx52.entity.OrdonnanceEntity;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public interface OrdonnanceService {

    public List<OrdonnanceEntity> findOrdonnanceByPatientId(Long patientId) ;

    public OrdonnanceEntity saveOrdonnance(String commentaire, String id, HashMap<String, String > actMaps, String soignantId, String patientId, Date heurePassage);

    List<OrdonnanceEntity> findOrdonnanceByHeurePassage(Date date1, Date date2);


}
