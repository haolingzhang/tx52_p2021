package fr.utbm.tx52.service;

import fr.utbm.tx52.entity.SoignantEntity;

public interface SoignantService {

    String getSoignantPasswordByEmail(String soignantEmail);

    SoignantEntity getSoignantByEmail(String soignantEmail);
}
