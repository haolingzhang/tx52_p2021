package fr.utbm.tx52.service;

import fr.utbm.tx52.entity.ActEntity;

import java.util.List;

public interface ActService {
    public List<ActEntity> findActByOrdonnanceId(Long ordonnanceId);
    public ActEntity findActByOrdonnanceIdAndActName(Long ordonnanceId, String actDescription);

}
