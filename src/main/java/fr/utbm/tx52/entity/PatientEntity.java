package fr.utbm.tx52.entity;

import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@DynamicInsert
@DynamicUpdate
@Table(name = "tb_patient")
@EntityListeners(AuditingEntityListener.class)
public class PatientEntity implements Serializable {

    private static final long serialVersionUID = -1785824516147698045L;

    /*
    id of patient
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    private String nom;
    private String prenom;
    private String age;
    private String sexe;
    private String address;
    private String phone;

    private String email;

    @Column(name="numero_societe")
    private String numeroSociete;

    private String commentaire;

    @Column(name="soignant_id")
    private Long soignantId;

//    @Column(name="heure_passage")
//    private Date heurePassage;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNumeroSociete() {
        return numeroSociete;
    }

    public void setNumeroSociete(String numeroSociete) {
        this.numeroSociete = numeroSociete;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public Long getSoignantId() {
        return soignantId;
    }

    public void setSoignantId(Long soignantId) {
        this.soignantId = soignantId;
    }

//    public Date getHeurePassage() {
//        return heurePassage;
//    }
//
//    public void setHeurePassage(Date heurePassage) {
//        this.heurePassage = heurePassage;
//    }
}
