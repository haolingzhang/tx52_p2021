package fr.utbm.tx52.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name = "tb_specialite_soignant")
public class SpecialiteSoignantEntity implements Serializable{


    private static final long serialVersionUID = -1785824516147698045L;

     /*
     id of patient
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @Column(name="soignant_id")
    private Long soignantId;

    @Column(name="specialite_id")
    private Long specialiteId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSoignantId() {
        return soignantId;
    }

    public void setSoignantId(Long soignantId) {
        this.soignantId = soignantId;
    }

    public Long getSpecialiteId() {
        return specialiteId;
    }

    public void setSpecialiteId(Long specialiteId) {
        this.specialiteId = specialiteId;
    }
}
