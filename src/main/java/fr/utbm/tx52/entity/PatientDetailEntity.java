package fr.utbm.tx52.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name = "tb_patient_detail")
public class PatientDetailEntity implements Serializable {

    private static final long serialVersionUID = -1785824516147698045L;

    /*
    id of patient_detail
    */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="patient_id")
    private Long patiendId;

    @Column(name="variable")
    private String variable;

    @Column(name="variable_value")
    private String variableValue;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPatiendId() {
        return patiendId;
    }

    public void setPatiendId(Long patiendId) {
        this.patiendId = patiendId;
    }

    public String getVariable() {
        return variable;
    }

    public void setVariable(String variable) {
        this.variable = variable;
    }

    public String getVariableValue() {
        return variableValue;
    }

    public void setVariableValue(String variableValue) {
        this.variableValue = variableValue;
    }
}
