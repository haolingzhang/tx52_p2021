package fr.utbm.tx52.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Data
@Table(name = "tb_specialite")
public class SpecialiteEntity implements Serializable {

    private static final long serialVersionUID = -1785824516147698045L;

    /*
    id of patient
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="specialite_name")
    private String specialiteName;

    @Column(name="specialite_description")
    private String specialiteDescription;

}
