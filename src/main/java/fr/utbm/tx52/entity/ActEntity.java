package fr.utbm.tx52.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name = "tb_act")
public class ActEntity  implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @Column(name="ordonnance_id")
    private Long ordonnanceId;

    @Column(name="act_description")
    private String actDescription;

    @Column(name="act_commentaire")
    private String actCommentaire;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrdonnanceId() {
        return ordonnanceId;
    }

    public void setOrdonnanceId(Long ordonnanceId) {
        this.ordonnanceId = ordonnanceId;
    }

    public String getActDescription() {
        return actDescription;
    }

    public void setActDescription(String actDescription) {
        this.actDescription = actDescription;
    }

    public String getActCommentaire() {
        return actCommentaire;
    }

    public void setActCommentaire(String actCommentaire) {
        this.actCommentaire = actCommentaire;
    }
}
