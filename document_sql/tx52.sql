-- MySQL dump 10.13  Distrib 8.0.24, for Win64 (x86_64)
--
-- Host: localhost    Database: tx52
-- ------------------------------------------------------
-- Server version	8.0.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tb_act`
--

DROP TABLE IF EXISTS `tb_act`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tb_act` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `act_description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `act_commentaire` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `ordonnance_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_act`
--

LOCK TABLES `tb_act` WRITE;
/*!40000 ALTER TABLE `tb_act` DISABLE KEYS */;
INSERT INTO `tb_act` VALUES (1,'température','37',1),(2,'oxygène du sang','98',1),(3,'température','37',2),(4,'oxygène du sang','98',2),(5,'température','37',3),(6,'oxygène du sang','98',3),(7,'température','37',4),(8,'oxygène du sang','98',4),(9,'température','36.5',6),(10,'oxygène du sang','98',6);
/*!40000 ALTER TABLE `tb_act` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_ordonnance`
--

DROP TABLE IF EXISTS `tb_ordonnance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tb_ordonnance` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `patient_id` bigint DEFAULT NULL,
  `soignant_id` bigint DEFAULT NULL,
  `commentaire` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `heure_passage` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_ordonnance`
--

LOCK TABLES `tb_ordonnance` WRITE;
/*!40000 ALTER TABLE `tb_ordonnance` DISABLE KEYS */;
INSERT INTO `tb_ordonnance` VALUES (1,1,1,'ca va','2021-05-12 00:00:00','2021-05-28 11:36:39','2021-05-28 10:00:00'),(2,1,1,'ca va','2021-05-12 00:00:00','2021-05-29 09:32:54','2021-05-29 09:00:00'),(3,1,1,'ca va bien','2021-05-12 00:00:00','2021-05-30 10:53:08','2021-05-30 10:00:00'),(4,1,1,'ca va',NULL,'2021-05-31 13:48:28','2021-05-31 10:00:00'),(5,2,1,NULL,'2021-05-31 00:00:00',NULL,'2021-05-31 11:00:00'),(6,3,1,'ca va','2021-05-31 00:00:00','2021-05-31 10:00:51','2021-05-31 10:00:00'),(7,4,1,NULL,'2021-05-31 00:00:00',NULL,'2021-05-31 14:30:00'),(8,1,1,NULL,'2021-05-31 00:00:00',NULL,'2021-06-01 10:00:00');
/*!40000 ALTER TABLE `tb_ordonnance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_patient`
--

DROP TABLE IF EXISTS `tb_patient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tb_patient` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'patient_id',
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) DEFAULT NULL,
  `age` varchar(255) DEFAULT NULL,
  `sexe` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `numero_societe` varchar(255) DEFAULT NULL,
  `commentaire` varchar(255) DEFAULT NULL,
  `soignant_id` bigint DEFAULT NULL,
  `heure_passage` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_patient`
--

LOCK TABLES `tb_patient` WRITE;
/*!40000 ALTER TABLE `tb_patient` DISABLE KEYS */;
INSERT INTO `tb_patient` VALUES (1,'Bernard','Anne','45','F','1 rue de parc','0611111111','anne.bernard@outlook.com','2100112325612',NULL,1,'2021-05-31 10:00:00'),(2,'Bernard','Beta','46','M','1 rue de mer','0611111112','beta.bernard@gmail.com','2100112325613',NULL,1,'2021-05-31 11:00:00'),(3,'Albert','Celine','47','F','2 rue de parc','0611111113','celine.albert@gmail.com','2100112325614',NULL,1,'2021-05-30 10:00:00'),(4,'Martin','Helen','48','F','2 rue de mer','0611111114','helen.martin@gmail.com','2100112325615',NULL,1,'2021-05-31 14:30:00');
/*!40000 ALTER TABLE `tb_patient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_patient_detail`
--

DROP TABLE IF EXISTS `tb_patient_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tb_patient_detail` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `patient_id` bigint NOT NULL,
  `variable` varchar(255) DEFAULT NULL,
  `variable_value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_patient_detail`
--

LOCK TABLES `tb_patient_detail` WRITE;
/*!40000 ALTER TABLE `tb_patient_detail` DISABLE KEYS */;
INSERT INTO `tb_patient_detail` VALUES (1,1,'groupe sanguin','O'),(2,1,'maladie héréditaire','non'),(3,2,'groupe sanguin','AB'),(4,2,'maladie héréditaire','maladie cardiaque'),(5,3,'groupe sanguin','B'),(6,3,'maladie héréditaire','non'),(7,4,'groupe sanguin','A'),(8,4,'maladie héréditaire','maladie cardiaque');
/*!40000 ALTER TABLE `tb_patient_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_soignant`
--

DROP TABLE IF EXISTS `tb_soignant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tb_soignant` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) DEFAULT NULL,
  `prenom` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_soignant`
--

LOCK TABLES `tb_soignant` WRITE;
/*!40000 ALTER TABLE `tb_soignant` DISABLE KEYS */;
INSERT INTO `tb_soignant` VALUES (1,'Martin','Celine','1 rue du people','0611111113','celine.martin@gmail.com','EFG@AB'),(2,'Martin','Bob','2 rue de parc','0611111114','bob.martin@gmai.com','EFG@AB'),(3,'',NULL,NULL,NULL,'admin','EFG@AB');
/*!40000 ALTER TABLE `tb_soignant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_specialite`
--

DROP TABLE IF EXISTS `tb_specialite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tb_specialite` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `specialite_name` varchar(255) DEFAULT NULL,
  `specialite_description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_specialite`
--

LOCK TABLES `tb_specialite` WRITE;
/*!40000 ALTER TABLE `tb_specialite` DISABLE KEYS */;
INSERT INTO `tb_specialite` VALUES (1,'clinique médicale',NULL),(2,'clinique chirurgicale',NULL);
/*!40000 ALTER TABLE `tb_specialite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_specialite_soignant`
--

DROP TABLE IF EXISTS `tb_specialite_soignant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tb_specialite_soignant` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `soignant_id` bigint NOT NULL,
  `specialite_id` bigint NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_specialite_soignant`
--

LOCK TABLES `tb_specialite_soignant` WRITE;
/*!40000 ALTER TABLE `tb_specialite_soignant` DISABLE KEYS */;
INSERT INTO `tb_specialite_soignant` VALUES (1,1,1),(2,1,2),(3,2,1);
/*!40000 ALTER TABLE `tb_specialite_soignant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'tx52'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-03 23:01:11
